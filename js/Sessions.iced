User = require '../models/User'
passport = require 'passport'
Sessions = 
  signin: (req, res, next) ->
    passport.authenticate('local', (err, user, info) ->
      if err
        req.flash 'err', info.message
        return next(err)
      if !user
        req.flash 'err', info.message
        return res.render('auth/sign-in', message: info.message)
      req.logIn user, (err) ->
        if err
          req.flash 'err', info.message
          return next(err)
        res.redirect '/dashboard'

  )(req, res, next)
  signup: (req, cb) ->
    data = 
      email: req.body.email
      name: req.body.name
      location: req.body.location
      iban: req.body.iban

    await User.register new User(data), req.body.password, defer err, user
    if user
      await req.logIn user, defer err
      if !err
        cb user
      else
        cb false, err.message
    else
      cb false, null

module.exports = Sessions