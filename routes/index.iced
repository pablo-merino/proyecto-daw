express = require 'express'
Config = require '../js/Config'
User = require '../models/User'
router = express.Router()
Sessions = require '../js/Sessions'
dashboardRoute = require './dashboard'

checkUser = (req, res, next) ->
  if req.user
    res.redirect '/dashboard'
  else
    next()

router.use '/', dashboardRoute

# Index GET route
router.get '/', (req, res) ->
  console.log req.user
  res.render 'index'

# Sign In GET route
router.get '/sign-in', checkUser, (req, res) ->
  res.render 'auth/sign-in'

# Sign In POST route
router.post '/sign-in', checkUser, (req, res, next) ->
  Sessions.signin(req, res, next)

# Sign Up GET route
router.get '/sign-up', checkUser, (req, res) ->
  res.render 'auth/sign-up'

# Sign Up POST route
router.post '/sign-up', checkUser, (req, res) ->
  Sessions.signup req, (user, err) ->
    if user
      res.redirect '/dashboard'
    else
      if err
        req.flash 'err', err
      else
        req.flash 'err', 'There was an error signing up'
      res.render 'auth/sign-up'

# Logout POST route
router.get '/sign-out', (req, res) ->
  req.logout()
  res.redirect('/')

module.exports = router
