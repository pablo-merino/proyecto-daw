express = require 'express'
Config = require '../js/Config'
router = express.Router()
User = require '../models/User'
Plugin = require '../models/Plugin'
Utils = require('../js/Utils')

checkUser = (req, res, next) ->
  if req.user
    next()
  else
    res.render 'error'

router.get '/dashboard', checkUser, (req, res) ->
  await Plugin.find uploader: req.user._id, defer err, plugins
  res.render 'dashboard', plugins: plugins

router.get '/dashboard/settings', checkUser, (req, res) ->
  res.render 'settings'

router.post '/dashboard/settings', checkUser, (req, res) ->
  updatedData =
    name: req.body.name
    location: req.body.location
    email: req.body.email
    iban: req.body.iban

  await User.update({_id: req.user._id}, updatedData).exec defer err
  if err
    req.flash 'err', 'Error updating the profile'
  else
    req.flash 'info', 'Success!'

  res.redirect '/dashboard/settings'

router.post '/dashboard/settings/password', checkUser, (req, res) ->
  pass = req.body.password
  await User.findOne {_id: req.user._id}, defer err, user
  await user.setPassword pass, defer err, user
  await user.save defer err
  if err
    req.flash 'err', 'Error updating the password'
  else
    req.flash 'info', 'Success!'

  res.redirect '/dashboard/settings'


module.exports = router
