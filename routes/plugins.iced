express = require('express')
Config = require('../js/Config')
router = express.Router()
Plugin = require '../models/Plugin'
User = require '../models/User'

checkUser = (req, res, next) ->
  if req.user
    next()
  else
    res.send 'wtf dude'

getPluginList = (req, res) ->
  page = 1
  if req.params.page
    page = req.params.page

  await Plugin.paginate {}, { page: page, limit: 10 }, defer err, result

  return res.render 'error' if !result

  prevPage = result.page - 1
  nextPage = result.page + 1

  if nextPage > result.pages
    nextPage = false

  if prevPage < 1
    prevPage = false

  if !nextPage && !prevPage
    needsHr = false
  else
    needsHr = true

  res.render 'plugins/index', plugins: result, nextPage: nextPage, prevPage: prevPage, needsHr: needsHr

getPluginsByOwner = (req, res) ->
  page = 1
  if req.params.page
    page = req.params.page

  await User.findOne slug: req.params.id, defer err, author
  console.log err, author
  return res.render 'error' if !author

  await Plugin.paginate {uploader: author._id}, { page: page, limit: 10 }, defer err, result
  return res.render 'error' if !result

  prevPage = result.page - 1
  nextPage = result.page + 1

  if nextPage > result.pages
    nextPage = false

  if prevPage < 1
    prevPage = false

  if !nextPage && !prevPage
    needsHr = false
  else
    needsHr = true

  res.render 'plugins/byauthor', author:author, plugins: result, nextPage: nextPage, prevPage: prevPage, needsHr: needsHr


router.get '/', getPluginList
router.get '/page/:page', getPluginList

router.get '/by/:id', getPluginsByOwner
router.get '/by/:id/page/:page', getPluginsByOwner

router.get '/new', checkUser, (req, res) ->
  res.render 'plugins/new'

router.post '/new', checkUser, (req, res) ->
  data = 
    name: req.body.name
    uploader: req.user._id
    description: req.body.description
    price: req.body.price
    shortcut: req.body.shortcut
    license: req.body.license
    short_description: req.body.shortdescription
    min_sketch_ver: req.body.minver
  plugin = new Plugin data

  await plugin.save defer err
  if err
    req.flash 'err', 'Error saving plugin: ' + err
    res.render 'plugins/new'
  else
    res.redirect '/plugins/' + plugin.slug

router.get '/:id', (req, res) ->
  await Plugin.findOne slug: req.params.id, defer err, plugin
  return res.render 'error' if !plugin
  await User.findOne _id: plugin.uploader, defer err, owner
  return res.render 'error' if !owner
  res.render 'plugins/show', plugin: plugin, owner: owner

module.exports = router
