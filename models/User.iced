mongoose = require 'mongoose'
slug = require 'mongoose-slug'
Schema = mongoose.Schema
passportLocalMongoose = require('passport-local-mongoose')

User = new Schema
  name: { type: String }
  location: 
    type: String
    default: 'The Earth'
  email: { type: String }
  plugins: 
    type: Array
    default: []
  iban: {type: String}

options =
  usernameField: 'email'
  usernameLowerCase: true
  limitAttempts: true
  maxAttempts: 5
  maxInterval: 1000*60
  incorrectUsernameError: 'Incorrect email or password'
  incorrectPasswordError: 'Incorrect email or password'
  missingUsernameError: 'Incorrect email or password'
  missingPasswordError: 'Incorrect email or password'

User.plugin passportLocalMongoose, options
User.plugin slug('name')

User.statics.serializeUser = () -> return (user, cb) -> cb null, user.id

User.statics.deserializeUser = () -> return (id, cb) => this.findOne {_id: id}, cb

module.exports = mongoose.model 'User', User