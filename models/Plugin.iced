mongoose = require('mongoose')
mongoosePaginate = require('mongoose-paginate')
slug = require 'mongoose-slug'

Schema = mongoose.Schema

Plugin = new Schema
  name: { type: String }
  description: { type: String }
  price: { type: String }
  uploader: Schema.Types.ObjectId
  license: { type: Number, default: 1 }
  short_description: { type: String }
  min_sketch_ver: { type: String }
  
Plugin.plugin mongoosePaginate
Plugin.plugin slug('name')

module.exports = mongoose.model 'Plugin', Plugin