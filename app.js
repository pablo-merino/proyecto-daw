// Modules
require('iced-coffee-script/register');
const express = require('express'),
      logger = require('morgan'),
      bodyParser = require('body-parser'),
      debug = require('debug')('sketch-marketplace'),
      path = require('path'),
      mongoose = require('mongoose'),
      passport = require('passport'),
      LocalStrategy = require('passport-local'),
      cookieParser = require('cookie-parser'),
      session = require('express-session'),
      flash = require('express-flash'),
      MongoStore = require('connect-mongo')(session);

// Routes
const indexRoute = require('./routes/index'),
      pluginsRoute = require('./routes/plugins');

// Config
const Config = require('./js/Config'),
      Handlebars = require('./js/Handlebars');

var app = express();

app.use(cookieParser('secret'));
app.use(session({
  proxy: true,
  resave: true,
  saveUninitialized: true,
  secret: 'lolailo',
  store: new MongoStore({
    url: Config.mongodb
  })
}));
app.use(flash());

app.use(passport.initialize());
app.use(passport.session());

// Models
const User = require('./models/User');
passport.use(User.createStrategy());
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

mongoose.connect(Config.mongodb);

app.engine('hbs', Handlebars.engine)
app.set('view engine', 'hbs')

app.use('/bower_components',  express.static(__dirname + '/bower_components'));
app.use('/public',  express.static(__dirname + '/public'));

app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: false}));

app.use(function (req, res, next) {
  if(req.user) {
    res.locals = {
     user: req.user
   };
  } 
  next();
});

app.use('/', indexRoute);
app.use('/plugins', pluginsRoute);

app.use(function(req, res){
  debug('404 - ' + req.path);
  res.status(404);
  res.render('error');
});

module.exports = app;
